package visual;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import core.Constants;
import core.Conway;

public class Gui extends JFrame{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static Conway conway;
	private JPanel pn_game, pn_buttons, pn_south, pn_north;
	private JButton btn_next,btn_start,btn_stop,btn_random,btn_reset;
	private Draw draw;
	private boolean isRunning;
	private Timer timer;
	
	/*
	 * Constructor setzt nimmt ein Objekt vom Typ Conway 
	 * als Parameter und ruft die init() funktion auf 
	 * um die GUI zu initialisieren und die Components zu erzeugen
	 */
	public Gui(Conway c) {
		super("Game Of Life");
		conway = c;
		init();
	}
	
	private void update() {
		conway.nextGen();
		draw.repaint();
	}

	/*
	 * init() rudt die erzeugenden Unterprogramme auf
	 */
	private void init() {
		createWindow();
		createPanels();
		ceateButtons();
		
		setVisible(true);
	}
	
	
	/*
	 * initWindow() erzeugt das Hauptfenster
	 */
	private void createWindow() {
		
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setSize(Constants.SIZE_WINDOW);
		setResizable(false);
		
		JPanel contentPane = new JPanel();
		
		contentPane.setLayout(new BorderLayout());
		setContentPane(contentPane);
		
	}
	
	/*
	 * initPanels() erzeugt die einzelnen JPanels und fügt sie dem Hauptfenster hinzu
	 */
	private void createPanels() {
		
		
		
		pn_buttons = new JPanel();
		pn_buttons.setLayout(new BoxLayout(pn_buttons, BoxLayout.X_AXIS));
		
		pn_north = new JPanel();
		pn_north.setBackground(Constants.COLOR_CONTRAST);
		pn_north.add(pn_buttons);
		
		draw = new Draw(conway);
		pn_game = new JPanel();
		pn_game.setBackground(Constants.COLOR_CONTRAST);
		pn_game.add(draw);
		
		pn_south = new JPanel();
		
		JLabel lbl_bottom = new JLabel("Conways Game Of Life - nschaefe");
		lbl_bottom.setForeground(Constants.COLOR_CELL);
		pn_south.add(lbl_bottom);
		pn_south.setBackground(Constants.COLOR_CONTRAST);
		
		
		
		
		getContentPane().add(pn_north, BorderLayout.PAGE_START);
		getContentPane().add(pn_game,BorderLayout.CENTER);
		getContentPane().add(pn_south,BorderLayout.SOUTH);
		
		
	}
	
	/*
	 * erzeugt die Buttons und fügt sie den entsprechenden Panels hinzu
	 */
	private void ceateButtons() {
		
		btn_next = new JButton("next");
	
		btn_next.addActionListener(new ActionListener() { 
			  public void actionPerformed(ActionEvent e) { 

				    update();
				  } 
				} );
		
		pn_buttons.add(btn_next);
		
		btn_start = new JButton("start");
		
		btn_start.addActionListener(new ActionListener() { 
			  public void actionPerformed(ActionEvent e) { 
				  startLoop();
				  }});
		
		pn_buttons.add(btn_start);
		
		btn_stop = new JButton("stop");
		
		btn_stop.addActionListener(new ActionListener() { 
			  public void actionPerformed(ActionEvent e) { 
				    stopLoop();
				  }});
		
		pn_buttons.add(btn_stop);
		
		btn_reset = new JButton("reset");
		
		btn_reset.addActionListener(new ActionListener() { 
			  public void actionPerformed(ActionEvent e) { 
				    conway.reset();
				    update();
				  }});
		
		
		pn_buttons.add(btn_reset);
		
		btn_random = new JButton("random");
				
		btn_random.addActionListener(new ActionListener() { 
			  public void actionPerformed(ActionEvent e) { 
				    conway.genesis((conway.getSize().height * conway.getSize().height)/2);
				    update();
				  }});
		
		pn_buttons.add(btn_random);
	}
	
	private void startLoop() {
		if(!isRunning) {
			timer = new Timer();
			timer.schedule(new TimerTask() {
				public void run() {
					update();
				}
			}, 0, (long) 100);
			isRunning = true;
		}
	}
	
	private void stopLoop() {
		if(timer != null){
			timer.cancel();
			isRunning = false;
		
		}
	}


}
