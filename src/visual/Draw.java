package visual;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JPanel;

import core.Constants;
import core.Conway;

public class Draw extends JPanel{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Conway conway;

	
	/*
	 * Erzeugt ein Kindobjekt von JPanel
	 * und überschreibt die Attribute/Funktionen
	 */
	Draw(Conway con){
		conway = con;
		 super.addMouseListener(mouseListener);
	}
	
	public void paintComponent(Graphics g) {
		
		super.paintComponent(g);
		
		setSize(Constants.SIZE_FIELD);
		setBounds(0,0,Constants.SIZE_FIELD.width, Constants.SIZE_FIELD.height);
		
		Graphics2D g2d = (Graphics2D) g;
		
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
		
		float pixelWidth = getPixelWidth();
	    float pixelHeight = getPixelHeight();
		
		for(int i = 0; i < conway.getSize().width; i += 1) {
			for(int j = 0; j < conway.getSize().height; j += 1) {
				float pixelX = i * getPixelWidth();
                float pixelY = j * getPixelHeight();
                
				g.setColor(conway.getMatrix()[i][j] ? Constants.COLOR_CELL : Constants.COLOR_BACKGROUND);
				
				
				g.fillRect(
                        Math.round(pixelX), 
                        Math.round(pixelY),
                        Math.round(pixelWidth), 
                        Math.round(pixelHeight));
			}
		}
	}
	
	private final MouseListener mouseListener = new MouseListener() {
        @Override
        public void mouseClicked(MouseEvent e) {
            int x = e.getX();
            int y = e.getY();
            
            float pixelWidth = getPixelWidth();
            float pixelHeight = getPixelHeight();
            
            int xIndex = (int)(x / pixelWidth);
            int yIndex = (int)(y / pixelHeight);
            conway.toggleCell(xIndex,yIndex);
           
            repaint();
            
        }

        /*
         * nicht benutzt
         */
		@Override
		public void mouseEntered(MouseEvent arg0) {}
		@Override
		public void mouseExited(MouseEvent arg0) {}
		@Override
		public void mousePressed(MouseEvent e) {}
		@Override
		public void mouseReleased(MouseEvent arg0) {}		
	};
	
    private float getPixelWidth() {
        float pixelWidth = (float)(getWidth() / conway.getSize().width);
        return pixelWidth;
    }
    
    private float getPixelHeight() {
        float pixelHeight = (float)(getHeight() / conway.getSize().height);
        return pixelHeight;
    }
}
