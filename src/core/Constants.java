package core;

import java.awt.Color;
import java.awt.Dimension;


/*
 * Constants Klasse mit statischen/finalen varibalen zur 
 * zentralen manipulation der wichtigen werte
 * 
 * analog zu css style sheets
 */
public class Constants {
	
	public final static Color COLOR_BACKGROUND = new Color(33,33,33);
	public final static Color COLOR_CELL = new Color(242,245,244);
	public final static Color COLOR_CONTRAST = new Color(238,77,46);
	
	public final static Dimension SIZE_CONWAY = new Dimension(60,60);
	public final static Dimension SIZE_WINDOW = new Dimension(600,700); 
	public final static Dimension SIZE_FIELD = new Dimension(600,600);
}
