package core;

import java.awt.Dimension;
import java.util.concurrent.ThreadLocalRandom;

public class Conway {
	
	private boolean matrix[][];
	private Dimension size;
	private int genCounter;
	
	/*
	 * Constructor erzeugt eine Matri in der Dimensio X*Y
	 * Intialsierung des primitiven Datentyps boolean
	 * standart: false
	 */
	public Conway (Dimension dim){
		size = dim;
		matrix = new boolean[size.width][size.height];
		genCounter = 0;
		
	}
	
	
	/*
	 * genesis() intialsiert den Automaten mit n zufälligen Zellen
	 */
	public void genesis(int count) {
		for(int i = 0; i < count; i += 1) {
			matrix[random(0,size.width)][random(0,size.height)]=true;
		}
	}
	
	/*
	 * reset() setzt den aktuellen Zustand aller Zellen auf false
	 */
	public void reset() {
		genCounter = 0;
		for(int i = 0; i < size.width; i += 1) {
			for(int j = 0; j < size.height; j += 1) {
				matrix[i][j] = false;
			}	
		}     
	}
	
	/*
	 * nextGen() durchläuft die Matrix und erstellt ein Abbild der Folgegenration
	 * und schreibt diese an Stelle der aktuellen Matrix
	 * Speicheraufwändig, jedoch robuts da keine nebeneffekte wie dirty read/write
	 */
	public void nextGen() {
			genCounter += 1;
			boolean nextState[][] = new boolean[size.width][matrix[0].length];
			
			for(int i = 0; i < size.width; i += 1) {
				for(int j = 0; j < size.height; j += 1) {
					
					nextState[i][j] = isAlive(i,j);
					
				}	
			}
			
			matrix = nextState;
		}
	
	
	
	/*
	 * isAlive prüft die Koordinaten der Matrix an der Stelle x,y
	 * zählt die Nachbarn und wendet die Regeln anhand der Nachbarn an
	 * als switch cases implementiert für mehr Übersicht
	 * und die Möglichkeit um weitere Regeln zu erweitern 
	 * sollte keiner der Fälle zu treffen, wird false als default gesetzt
	 */
	public boolean isAlive(int x, int y) {
		int n = countNeighbors(x, y);
		
		switch (n) {

		case 2:
			return matrix[x][y] ? true:false;
		
		case 3:
			return true;
		
//		case 4:
//			return null;
			
		default:
			return false;
			
		}
	}
	
	/*
	 * countNeighbors() prüft alle Nachbarn von der Zelle x,y
	 * ist die Zelle am Leben wird der Counter auf -1 Gesetzt
	 * damit diese den Counter nicht erhöht (reflexiv)
	 * 
	 * die schleifen laufen von -1 bis 1 um die direkte Umgebung zu Untersuchen
	 * die Morduloperation schafft erzeugt den "Wrapper" so das anliegende Zellen
	 * über den Rand hinaus gezählt werden
	 */
	public int countNeighbors(int x, int y) {
		int counter = matrix[x][y] ? -1 : 0;
		
		for(int i = -1; i < 2; i += 1) {
			for(int j = -1; j < 2; j+= 1) {
				if(matrix[(size.width+x+i)%size.width][(size.height+y+j)%size.height]) {counter += 1;}
			}
		}
		return counter;
	}
	
	//Getter/ Setter
	public boolean[][] getMatrix(){
		return matrix;
	}
	public Dimension getSize() {
		return size;
	}
	public int getCounter() {
		return genCounter;
	}
	public void setMatrix(boolean mat[][]) {
		this.matrix = mat; 
	}
	
	
	public void toggleCell(int x, int y) {
		matrix[x][y] = !matrix[x][y];
	}
	
	

	/*
	 * random() erzeugt eine Zufälligen int im Intervall [start,end]
	 * ThreadLocalRandom vermeidet Nebeneffekte und sorgt für Robustheit
	 */
	private int random(int start, int end) {
		return ThreadLocalRandom.current().nextInt(start, end);
	}
	
}
