package core;

import javax.swing.SwingUtilities;

import visual.Gui;

public class Main {

	public static void main(String[] args) {
		
		
		
		
		SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
            	
        		 new Gui(new Conway(Constants.SIZE_CONWAY));
            }
        });
	}

}
