package test;


import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import java.awt.Dimension;

import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;

import core.Conway;
import visual.Gui;


public class TestConway {
	private Conway conway;
	private Gui gui;
	
	@Before
	public void initConway() {
		conway = new Conway(new Dimension(5,5));
		
	}
	
	@Test
	@DisplayName("testing initialization")
	public void testInit() {
		
		assertNotNull("matrix not null",conway.getMatrix());
		
		assertArrayEquals("matrix initialised",new boolean[][]  
		{
				{false, false, false,false, false},
				{false, false, false,false, false},
				{false, false, false,false, false},
				{false, false, false,false, false},
				{false, false, false,false, false}
		}, conway.getMatrix());
	}
	
	@Test
	@DisplayName("testing counting neighbours")
	public void testCountingNeighbours() {
		conway.setMatrix(new boolean[][]  
		{
				{false, false, false},
				{false, true, false},
				{false, false, false}
		});
		
		assertEquals(0, conway.countNeighbors(1,1));
		
		
		conway.setMatrix(new boolean[][]  
		{
				{true, false, true},
				{false, true, false},
				{true, false, true}
		});
				
		assertEquals(4, conway.countNeighbors(1,1));
		
		
		conway.setMatrix(new boolean[][]  
		{
				{false, true, false},
				{true, true, true},
				{false, true, false}
		});
				
		assertEquals(4, conway.countNeighbors(1,1));
		
		conway.setMatrix(new boolean[][]  
		{
				{true, true, true},
				{true, true, true},
				{true, true, true}
		});
				
		assertEquals(8, conway.countNeighbors(1,1));
	}
	
	
	@Test
	@DisplayName("testing rules")
	public void testRules() {
		conway.setMatrix(new boolean[][]{
			{false, false, false, false, false},
			{false, true, false, true, false},
			{false, false, false, false, false},
			{false, false, true,false, false},
			{false, false, false,false, false}
		});
		
		conway.nextGen();
		
		assertArrayEquals("testing rule 1",new boolean[][] {
			{false, false, false,false, false},
			{false, false, false,false, false},
			{false, false, true,false, false},
			{false, false, false,false, false},
			{false, false, false,false, false}
		}, conway.getMatrix());
		
		conway.setMatrix(new boolean[][]{
			{false, false, false, false, false},
			{false, false, false, false, false},
			{false, false, true, true, false},
			{false, false, false, false, false},
			{false, false, false, false, false}
		});
		
		conway.nextGen();
		
		assertArrayEquals("testing rule 2",new boolean[][] {
			{false, false, false, false, false},
			{false, false, false, false, false},
			{false, false, false, false, false},
			{false, false, false, false, false},
			{false, false, false, false, false}
		}, conway.getMatrix());
		
		conway.setMatrix(new boolean[][]{
			{false, false, false, false, false},
			{false, true, false, false, false},
			{false, false, true, false, false},
			{false, false, false, true, false},
			{false, false, false, false, false}
		});
		
		conway.nextGen();
		
		assertArrayEquals("testing rule 3",new boolean[][] {
			{false, false, false, false, false},
			{false, false, false, false, false},
			{false, false, true, false, false},
			{false, false, false, false, false},
			{false, false, false, false, false}
		}, conway.getMatrix());
		
		conway.setMatrix(new boolean[][]{
			{false, false, false, false, false},
			{false, true, false, true, false},
			{false, false, true, false, false},
			{false, true, false, true, false},
			{false, false, false, false, false}
		});
		
		conway.nextGen();
		
		assertArrayEquals("testing rule 4",new boolean[][] {
			{false, false, false, false, false},
			{false, false, true, false, false},
			{false, true, false, true, false},
			{false, false, true, false, false},
			{false, false, false, false, false}
		}, conway.getMatrix());
	}
	
	@Test
	@DisplayName("testing edge behaviour")
	public void testEdgeBehav() {
		conway.setMatrix(new boolean[][]{
			{true, false, false, true, true},
			{false, false, false, false, false},
			{false, false, false, false, false},
			{true, false, false, true, true},
			{true, false, false, true, true}
		});
		
		assertEquals(8, conway.countNeighbors(4,4),"counting neighbours of p(4,4)");
		
		conway.nextGen();
		
		assertArrayEquals("testing edge behaviour",new boolean[][] {
			{true, false, false, true, false},
			{false, false, false, false, true},
			{false, false, false, false, true},
			{true, false, false, true, false},
			{false, true, true, false, false}
		}, conway.getMatrix());	
	}
	
	
	@Test
	@DisplayName("testing genesis and reset functions")
	public void testMore() {
		conway = new Conway(new Dimension(10,10));
		boolean testMatrix[][] = new boolean[10][10];
		
		assertArrayEquals(testMatrix, conway.getMatrix());
		
		conway.genesis(25);
		
		
		assertNotEquals(testMatrix, conway.getMatrix());
		testMatrix = conway.getMatrix();
		
		conway.nextGen();
		assertNotEquals(testMatrix, conway.getMatrix());
		
		conway.reset();
		
		testMatrix = new boolean[10][10];
		
		assertArrayEquals(testMatrix, conway.getMatrix());
		
		                  
	}
	
	@Test
	@DisplayName("testing cell toggle")
	
	public void testToggle() {
		conway.toggleCell(2, 2);
		assertArrayEquals(new boolean[][] {
			{false, false, false,false, false},
			{false, false, false,false, false},
			{false, false, true,false, false},
			{false, false, false,false, false},
			{false, false, false,false, false}
		}, conway.getMatrix());
	}
	
	
	@Test
	@DisplayName("testing gui")
	public void testGuiInit() {
		gui = new Gui(conway);
		assertNotNull(gui);
	}
}
